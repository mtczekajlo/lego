from threading import Thread
from queue import Queue
from approxeng.input.selectbinder import ControllerResource
from time import sleep
from copy import deepcopy


class Pad(Thread):
    throttle_levels = 7

    def __init__(self, pad_queue: Queue, bucket: Queue):
        super().__init__()
        self.pad_queue = pad_queue
        self.bucket = bucket
        self.analogs = {"ly": 0, "ry": 0}
        self.old_analogs = self.analogs

    def run(self) -> None:
        while True:
            try:
                with ControllerResource(dead_zone=0.1, hot_zone=0.05) as pad:
                    print(f'Pad connected: {repr(pad)}')
                    while pad.connected:
                        self.handle({"ly": pad.ly, "ry": pad.ry}, pad.check_presses())
                        sleep(0.05)
            except IOError:
                print('No pad connected...')
                sleep(1)
            except Exception as e:
                self.bucket.put(e)

    def handle(self, analogs, buttons):
        send_to_queue = False
        if len(buttons.names):
            send_to_queue = True

        self.analogs = deepcopy(analogs)
        for (nk, nv) in self.analogs.items():
            nv = int(nv / (1 / self.throttle_levels))
            self.analogs[nk] = nv
        if self.old_analogs != self.analogs:
            self.old_analogs = self.analogs
            send_to_queue = True

        if send_to_queue:
            self.pad_queue.put((self.analogs, buttons))
