from threading import Thread, Timer
from queue import Queue, Empty
from source.libs import pyslinger


class IrBlaster(Thread):
    def __init__(self, packet_queue: Queue, bucket: Queue, gpio_pin: int = 24, timer_period: float = 0.5):
        super().__init__()
        self.packet_queue = packet_queue
        self.bucket = bucket
        self.timer_period = timer_period
        self.timer = self._create_timer()
        self.last_packet = None
        self.ir = pyslinger.IR(gpio_pin)

    def _create_timer(self):
        return Timer(self.timer_period, self._resend)

    def _resend(self) -> None:
        self.packet_queue.put((self.last_packet, True))

    def _send_code(self, code):
        formatted = format(code, '016b')
        self.ir.send_code(formatted)

    def _main_handler(self, data):
        self._send_code(data)

    def run(self) -> None:
        while True:
            try:
                self.last_packet, repetitive = self.packet_queue.get(timeout=0.001)
                self.timer.cancel()
                self._main_handler(self.last_packet)
                if repetitive:
                    self.timer = self._create_timer()
                    self.timer.start()
            except Empty:
                pass
            except Exception as e:
                self.bucket.put(e)
