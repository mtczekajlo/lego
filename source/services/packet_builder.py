from source.libs.lego_packet_builder import LegoPacketBuilder

from threading import Thread
from queue import Queue, Empty


class PacketWorker(Thread):
    def __init__(self, pad_queue: Queue, packet_queue: Queue, bucket: Queue):
        super().__init__()
        self.pb = LegoPacketBuilder(0)
        self.pad_queue = pad_queue
        self.packet_queue = packet_queue
        self.handler_map = {
            'home': self.pb.toggle_channel,
            'select': self.pb.toggle_mode,
            'r1': self.pb.gear_up,
            'l1': self.pb.gear_down,
            'dleft': self.pb.invert_left,
            'dright': self.pb.invert_right,
            'triangle': self.pb.swap_channels,
            'square': self._kill_thread
        }
        self.bucket = bucket

    def _kill_thread(self):
        raise Exception("Commiting suicide!")

    def run(self) -> None:
        while True:
            try:
                analogs, buttons = self.pad_queue.get(timeout=0.01)
                print(analogs, buttons)
                if buttons:
                    for button in buttons:
                        try:
                            self.handler_map[button]()
                        except KeyError:
                            pass
                if analogs:
                    repetitive = bool(analogs["ly"] or analogs["ry"])
                    self.packet_queue.put((self.pb.multi_builder(analogs['ly'], analogs['ry']), repetitive))
            except Empty:
                pass
            except Exception as e:
                self.bucket.put(e)
