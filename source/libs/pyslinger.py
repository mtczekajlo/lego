import pigpio

from copy import copy


class WaveGenerator:
    def __init__(self, protocol):
        self.protocol = protocol
        self.pulses = []

    def add_pulse(self, gpio_on, gpio_off, us_delay):
        self.pulses.append(pigpio.pulse(gpio_on, gpio_off, us_delay))

    def zero(self, duration):
        self.add_pulse(0, 1 << self.protocol.master.gpio_pin, duration)

    def one(self, duration):
        period_time = 1_000_000.0 / self.protocol.frequency
        on_duration = int(round(period_time * self.protocol.duty_cycle))
        off_duration = int(round(period_time * (1.0 - self.protocol.duty_cycle)))
        total_periods = int(round(duration / period_time))
        total_pulses = total_periods * 2

        for i in range(total_pulses):
            if i % 2 == 0:
                self.add_pulse(1 << self.protocol.master.gpio_pin, 0, on_duration)
            else:
                self.add_pulse(0, 1 << self.protocol.master.gpio_pin, off_duration)


class LegoProtocol:
    def __init__(self,
                 master,
                 frequency=38000,
                 duty_cycle=0.5,
                 pulse_duration=int(6 * 1000 / 38),
                 start_stop_gap_duration=int(39 * 1000 / 38),
                 one_gap_duration=int(21 * 1000 / 38),
                 zero_gap_duration=int(10 * 1000 / 38)):
        self.master = master
        self.wave_generator = WaveGenerator(self)
        self.frequency = frequency
        self.duty_cycle = duty_cycle
        self.pulse_duration = pulse_duration
        self.start_stop_gap_duration = start_stop_gap_duration
        self.one_gap_duration = one_gap_duration
        self.zero_gap_duration = zero_gap_duration
        self.funcs = {
            "0": self.zero,
            "1": self.one,
        }

    def process_code(self, ircode):
        self.wave_generator.pulses.clear()
        self.start_stop()
        for i in ircode:
            self.funcs[i]()
        self.start_stop()
        return copy(self.wave_generator.pulses)

    def start_stop(self):
        self.wave_generator.one(self.pulse_duration)
        self.wave_generator.zero(self.start_stop_gap_duration)

    def zero(self):
        self.wave_generator.one(self.pulse_duration)
        self.wave_generator.zero(self.zero_gap_duration)

    def one(self):
        self.wave_generator.one(self.pulse_duration)
        self.wave_generator.zero(self.one_gap_duration)


class IR:
    def __init__(self, gpio_pin):
        self.pigpio = pigpio.pi()
        self.gpio_pin = gpio_pin
        self.pigpio.set_mode(self.gpio_pin, pigpio.OUTPUT)
        self.protocol = LegoProtocol(self)
        print("IR ready")

    def send_code(self, ircode):
        pulses = self.protocol.process_code(ircode)
        if not len(pulses):
            raise Exception("Error in processing IR code!")
        clear = self.pigpio.wave_clear()
        if clear != 0:
            raise Exception("Error in clearing wave!")
        pulses = self.pigpio.wave_add_generic(pulses)
        if pulses < 0:
            raise Exception("Error in adding wave!")
        wave_id = self.pigpio.wave_create()
        if wave_id >= 0:
            result = self.pigpio.wave_send_once(wave_id)
            if result < 0:
                raise Exception(f"Error! result:{result}")
        else:
            raise Exception(f"Error creating wave: {wave_id}")
        while self.pigpio.wave_tx_busy():
            pass
        self.pigpio.wave_delete(wave_id)
