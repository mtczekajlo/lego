from copy import copy


class LegoPacketBuilder:
    toggle_bit = bool()
    escape_bit = bool()
    channel_semi_nibble = int()
    nibble_2 = int()
    nibble_3 = int()
    lrc = int()
    pwm_map = {
        0: 0b0000,
        1: 0b0001,
        2: 0b0010,
        3: 0b0011,
        4: 0b0100,
        5: 0b0101,
        6: 0b0110,
        7: 0b0111,
        # 'break': 0b1000,  # TODO: how to handle active breaking?
        -7: 0b1001,
        -6: 0b1010,
        -5: 0b1011,
        -4: 0b1100,
        -3: 0b1101,
        -2: 0b1110,
        -1: 0b1111,
    }

    direct_map = {
        0: 0b00,
        1: 0b01,
        -1: 0b10,
        # 'break': 0b11,  # TODO: how to handle active breaking?
    }

    def __init__(self, channel: int = 0):
        self.channel_semi_nibble = channel
        self.mode = 0
        self.modes = {0: self.build_combo_pwm, 1: self.build_combo_direct}
        self.default_clamps = {0: 7, 1: 1}
        self.clamps = copy(self.default_clamps)
        self.blue_inverted = 0
        self.red_inverted = 0
        self.channels_swapped = False

    def _build(self):
        packet = bytearray()
        packet.append(self.toggle_bit << 3 | self.escape_bit << 2 | self.channel_semi_nibble)
        packet.append(self.nibble_2)
        packet.append(self.nibble_3)
        packet.append(0xF ^ packet[0] ^ packet[1] ^ packet[2])
        packet = int('{:X}{:X}{:X}{:X}'.format(packet[0], packet[1], packet[2], packet[3]), base=16)
        return packet

    @staticmethod
    def _clamp(n, minn, maxn):
        return max(min(maxn, n), minn)

    def _clamp_channels(self, blue, red):
        if blue != 'break':
            blue = self._clamp(blue, -self.clamps[self.mode], self.clamps[self.mode])
        if red != 'break':
            red = self._clamp(red, -self.clamps[self.mode], self.clamps[self.mode])
        return blue, red

    def build_combo_direct(self, blue: int, red: int):
        blue, red = self._clamp_channels(blue, red)
        self.escape_bit = 0
        self.nibble_2 = 1
        self.nibble_3 = self.direct_map[blue] << 2 | self.direct_map[red]
        packet = self._build()
        # self.toggle_bit = self.toggle_bit ^ 1  # TODO: is it needed?
        return packet

    def build_combo_pwm(self, blue: int, red: int):
        blue, red = self._clamp_channels(blue, red)
        self.escape_bit = 1
        self.nibble_2 = self.pwm_map[blue]
        self.nibble_3 = self.pwm_map[red]
        packet = self._build()
        return packet

    def multi_builder(self, blue: int, red: int):
        if self.channels_swapped:
            blue, red = red, blue
        if self.blue_inverted:
            blue = blue * -1
        if self.red_inverted:
            red = red * -1
        return self.modes[self.mode](blue, red)

    def toggle_channel(self):
        self.channel_semi_nibble += 1
        if self.channel_semi_nibble > 3:
            self.channel_semi_nibble = 0
        print(f"Changed IR channel to: {self.channel_semi_nibble + 1}")
        return self.channel_semi_nibble

    def gear_up(self):
        if self.clamps[self.mode] < self.default_clamps[self.mode]:
            self.clamps[self.mode] += 1
        print(f"Max gear set to: {self.clamps[self.mode]}")

    def gear_down(self):
        if self.clamps[self.mode] > 1:
            self.clamps[self.mode] -= 1
        print(f"Max gear set to: {self.clamps[self.mode]}")

    def toggle_mode(self):
        self.mode = self.mode ^ 1
        print(f"Changed mode to: {self.modes[self.mode].__name__}")
        print(f"Changed clamp to: {self.clamps[self.mode]}")

    def invert_left(self):
        self.blue_inverted = self.blue_inverted ^ 1
        print(f"Blue channel inverted: {bool(self.blue_inverted)}")

    def invert_right(self):
        self.red_inverted = self.red_inverted ^ 1
        print(f"Red channel inverted: {bool(self.blue_inverted)}")

    def swap_channels(self):
        self.channels_swapped ^= True
        print(f"Channels swapped: {self.channels_swapped}")
