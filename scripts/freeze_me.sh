#!/bin/bash
set -xEeuo pipefail

cd "$(dirname "$(readlink -f "$0")")/.."

if [ ! -v VIRTUAL_ENV ]; then
  source venv/bin/activate
fi

python3 -m pip freeze >requirements.txt
