#!/bin/bash
set -xEeuo pipefail

sudo dnf install -y python3-devel \
  gcc \
  kernel-headers-"$(uname -r)"
