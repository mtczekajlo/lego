#!/bin/bash
set -xEeuo pipefail

cd "$(dirname "$(readlink -f "$0")")/.."

rm -rf -- venv
python3 -m venv --clear venv
source venv/bin/activate

python3 -m pip install --upgrade pip setuptools wheel

for line in $(<requirements.txt); do
  python3 -m pip install "${line}"
done
