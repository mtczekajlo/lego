#!/bin/bash
set -xEeuo pipefail

mkdir -p 3rd_party
cd 3rd_party

function git_up() {
  (
    cd "$1"
    git reset --hard HEAD
    git checkout master
    git pull -r
  )
}

git clone https://github.com/joan2937/pigpio.git || git_up pigpio
git clone https://github.com/prcjac/python-lirc-send || git_up python-lirc-send
git clone https://github.com/iConor/lego-lirc || git_up lego-lirc
git clone https://github.com/bschwind/ir-slinger || git_up ir-slinger
git clone https://github.com/atar-axis/xpadneo.git || git_up xpadneo
