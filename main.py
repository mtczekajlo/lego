#!/usr/bin/env python3

from source.services import gamepad
from source.services import packet_builder
from source.services import ir_blaster
from queue import Queue
import os

if __name__ == '__main__':
    pad_queue = Queue()
    packet_queue = Queue()
    bucket = Queue()

    pad = gamepad.Pad(pad_queue, bucket)
    packet_worker = packet_builder.PacketWorker(pad_queue, packet_queue, bucket)
    ir_blaster = ir_blaster.IrBlaster(packet_queue, bucket)

    packet_worker.start()
    pad.start()
    ir_blaster.start()

    while True:
        try:
            e = bucket.get(block=True)
            print(repr(e), e)
            os.kill(os.getpid(), 9)
        except KeyboardInterrupt as e:
            bucket.put(e)
