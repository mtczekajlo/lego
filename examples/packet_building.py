#!/usr/bin/env python3

from source.libs import lego_packet_builder


def print_func(func):
    print('###', func.__name__, '###')


if __name__ == '__main__':
    pb = lego_packet_builder.LegoPacketBuilder()

    print_func(pb.build_combo_direct)
    for y in range(1, -2, -1):
        for x in range(-1, 2):
            print(hex(pb.build_combo_direct(x, y)), f'{x:+}', f'{y:+}', end='\t')
        print('')

    print_func(pb.build_combo_pwm)
    for y in range(7, -8, -1):
        for x in range(-7, 8):
            print(hex(pb.build_combo_pwm(x, y)), f'{x:+}', f'{y:+}', end='\t')
        print('')
